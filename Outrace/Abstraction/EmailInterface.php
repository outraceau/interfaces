<?php
declare(strict_types=1);

namespace Outrace\Abstraction;

interface EmailInterface
{
    /**
     * @return string
     */
    public function getSubject(): string;

    /**
     * @return string
     */
    public function getBody(): string;

    /**
     * @return string
     */
    public function getTo(): string;

    /**
     * @return string
     */
    public function getFrom(): string;
}
