<?php
declare(strict_types=1);

namespace Outrace\Abstraction;

interface SmsInterface
{
    /**
     * @return string
     */
    public function getBody(): string;

    /**
     * @return string
     */
    public function getTo(): string;

    /**
     * @return string
     */
    public function getFrom(): string;
}
